package nl.something.homepage;

import homepage.HomepageApplication
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [HomepageApplication::class]
)
class HomepageApplicationTest(@Autowired private val restTemplate: TestRestTemplate) {

    @Test
    fun `context should load`() {

    }
}
