FROM maven:openjdk AS builder

RUN mkdir -p /usr/src/homepage-api/
WORKDIR /usr/src/homepage-api
COPY . .

RUN mvn clean package

FROM openjdk:alpine
RUN addgroup -S spring && adduser -h /var/spring -S -G spring spring && chmod 700 /var/spring
WORKDIR /var/spring
COPY --from=builder --chown=spring:spring /usr/src/homepage-api/target/*.jar app.jar
RUN chmod 400 app.jar
USER spring:spring
CMD ["java", "-jar", "app.jar"]
